# TorGhost Ubuntu 20.04

# original repo https://github.com/SusmithKrishnan/torghost

It has some bugs when I was trying to run it on Ubuntu 20.04 . 
So I made a little changes of the original script ( torghost.py ) .

# Build from SusmithKrishnan repo

#### > clone original repo
#### > sudo apt install python-pip 
#### > sudo apt install python3-pip
#### > sudo pip install requests
#### > sudo pip install stem
#### > sudo apt install cython3
( I did it manually to avoid further errrors )

#### > cd torghost/
#### > replace original torghost.py file with the one in this repo
#### > chmod +x build.sh
#### > ./build.sh

## ( run it sudo ) ; usage :

#### sudo torghost -s ( starts TorGhost )
#### sudo torghost -r ( Request new tor exit node )
#### sudo torghost -x ( Stop Torghost )
#### sudo torghost -h ( print this help and exit )

I had to remove upgrade option cause it was causing some errors .
That was my first idea and it ran normally , so I left it that way .
In future I may fix it , if a new version is released .

# Special thanks to SusmithKrishnan 
